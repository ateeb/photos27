package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Controller for searchInput.fxml
 * @author Daanyal and Ateeb
 */
public class SearchInputController implements Initializable {

    @FXML Button logout;
    @FXML Button quitButton;
    @FXML Button backButton;
    @FXML Button dateSearch;
    @FXML DatePicker startDate;
    @FXML DatePicker endDate;
    @FXML ChoiceBox<String> operator;
    @FXML ChoiceBox tagOne;
    @FXML ChoiceBox tagTwo;
    @FXML Button searchOne;
    @FXML Button searchTwo;
    @FXML TextField valueOne;
    @FXML TextField valueTwo;

    public static ArrayList<Photo> results;

    /**
     * Initializes and sets up events handlers and fx controls such as buttons and views
     * @param url url
     * @param rb resource bundle
     */
    public void initialize(URL url, ResourceBundle rb) {

        results = new ArrayList<Photo>();
        ArrayList<String> t = new ArrayList<String>();
        t.add("AND");
        t.add("OR");
        ObservableList<String> ops = FXCollections.observableArrayList();
        ops.addAll(t);
        operator.setItems(ops);
        loadTypes();

        EventHandler<ActionEvent> searchByDate = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                ArrayList<Album> albums = Album.getAlbumsByUserForEditing(LoginController.user.toLowerCase());
                results = new ArrayList<Photo>();
                LocalDate start = startDate.getValue();
                LocalDate end = endDate.getValue();

                if(start == null || end == null){
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "You did not select dates. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
                else {
                    for (Album album : albums) {
                        for (Photo p : album.images) {
                            if (!results.contains(p) && p.timestamp.isAfter(start) && p.timestamp.isBefore(end)) {
                                results.add(p);
                            }
                        }
                    }

                    goToResults();
                }
            }
        };
        dateSearch.setOnAction(searchByDate);

        EventHandler<ActionEvent> searchonetag = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                ArrayList<Album> albums = Album.getAlbumsByUserForEditing(LoginController.user.toLowerCase());
                results = new ArrayList<Photo>();

                if(tagOne.getSelectionModel().getSelectedItem() == null || valueOne.getCharacters().toString().equalsIgnoreCase("")){
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "You did not select appropriate tags. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
                else {
                    Tag tag = new Tag(tagOne.getSelectionModel().getSelectedItem().toString(), valueOne.getCharacters().toString(), false);
                    for (Album album : albums) {
                        for (Photo p : album.images) {
                            if(!results.contains(p) && p.hasTag(tag)){
                                results.add(p);
                            }
                        }
                    }

                    goToResults();
                }
            }
        };
        valueOne.setOnAction(searchonetag);
        searchOne.setOnAction(searchonetag);

        EventHandler<ActionEvent> searchtwotag = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                ArrayList<Album> albums = Album.getAlbumsByUserForEditing(LoginController.user.toLowerCase());
                results = new ArrayList<Photo>();

                if(operator.getSelectionModel().getSelectedItem().equalsIgnoreCase("") || tagOne.getSelectionModel().getSelectedItem() == null || tagTwo.getSelectionModel().getSelectedItem() == null || valueOne.getCharacters().toString().equalsIgnoreCase("") || valueTwo.getCharacters().toString().equalsIgnoreCase("")){
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "You did not select appropriate tags. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
                else {
                    Tag tag = new Tag(tagOne.getSelectionModel().getSelectedItem().toString(), valueOne.getCharacters().toString(), false);
                    Tag tag2 = new Tag(tagTwo.getSelectionModel().getSelectedItem().toString(), valueTwo.getCharacters().toString(), false);
                    String op = operator.getSelectionModel().getSelectedItem();

                    for (Album album : albums) {
                        for (Photo p : album.images) {
                            if(op.equalsIgnoreCase("AND")){
                                if(!results.contains(p) && p.hasTag(tag) && p.hasTag(tag2)){
                                    results.add(p);
                                }
                            }
                            else if(op.equalsIgnoreCase("OR")){
                                if(!results.contains(p) && (p.hasTag(tag) ^ p.hasTag(tag2))){
                                    results.add(p);
                                }
                            }
                        }
                    }

                    goToResults();
                }
            }
        };
        valueTwo.setOnAction(searchtwotag);
        searchTwo.setOnAction(searchtwotag);

        EventHandler<ActionEvent> back = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Album.current = null;

                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/user.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        backButton.setOnAction(back);

        EventHandler<ActionEvent> backtologin = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/login.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        logout.setOnAction(backtologin);

        EventHandler<ActionEvent> quitnow = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) quitButton.getScene().getWindow();
                stage.close();
            }
        };
        quitButton.setOnAction(quitnow);

    }

    public void goToResults(){
        if(results.isEmpty()){
            Alert alertExists = new Alert(Alert.AlertType.WARNING, "This search returned no results. Please try again.", ButtonType.OK);
            alertExists.showAndWait();

            return;
        }

        Stage window = (Stage) logout.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("../view/search.fxml"));
        try {
            window.setScene(new Scene(loader.load()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void loadTypes(){
        ObservableList<String> ol = FXCollections.observableArrayList();

        Tag.tags = Tag.getTagsForEdit();
        for (Tag t : Tag.defaultTags) {
            ol.add(t.getName());
        }
        for (Tag t : Tag.tags ){
            ol.add(t.getName());
        }

        tagOne.setItems(ol);
        tagTwo.setItems(ol);
    }

}
