package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Album;
import model.Photo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Controller for search.fxml
 * @author Daanyal and Ateeb
 */
public class SearchController implements Initializable {

    @FXML ListView lv;
    @FXML Button logout;
    @FXML Button quitButton;
    @FXML Button backButton;
    @FXML Label details;
    @FXML ImageView imageDisplay;
    @FXML Button lastButton;
    @FXML Button nextButton;
    @FXML TextField newAlbum;
    @FXML Button create;

    /**
     * Initializes and sets up events handlers and fx controls such as buttons and views
     * @param url url
     * @param rb resource bundle
     */
    public void initialize(URL url, ResourceBundle rb) {

        loadResults();

        EventHandler<ActionEvent> addnewalbum = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Album t = Album.newAlbum(newAlbum.getCharacters().toString(), LoginController.user);
                if(t == null){
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "You already have an album of this name. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
                else{
                    for(Photo p : SearchInputController.results){
                        t.addImage(p);
                    }

                    SearchInputController.results = new ArrayList<Photo>();

                    Stage window = (Stage) logout.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("../view/user.fxml"));
                    try {
                        window.setScene(new Scene(loader.load()));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };
        create.setOnAction(addnewalbum);
        newAlbum.setOnAction(addnewalbum);

        EventHandler<ActionEvent> back = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                SearchInputController.results = new ArrayList<Photo>();

                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/user.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        backButton.setOnAction(back);

        EventHandler<ActionEvent> backtologin = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/login.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        logout.setOnAction(backtologin);

        EventHandler<ActionEvent> quitnow = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) quitButton.getScene().getWindow();
                stage.close();
            }
        };
        quitButton.setOnAction(quitnow);

        //Link display
        lv.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Photo>() {
                    public void changed(ObservableValue<? extends Photo> ov,
                                        Photo old_val, Photo new_val) {
                        if(old_val != new_val && new_val != null) {
                            if (!lv.getItems().isEmpty()) {
                                details.setText(new_val.getDisplay());
                                imageDisplay.setImage(new_val.loadImage());
                            }
                        }
                    }
                });

        //Arrow keys
        EventHandler<ActionEvent> next = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                lv.requestFocus();
                lv.getSelectionModel().select(lv.getSelectionModel().getSelectedIndex() + 1);
                Photo.current = (Photo)lv.getSelectionModel().getSelectedItem();
            }
        };
        nextButton.setOnAction(next);
        EventHandler<ActionEvent> last = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                lv.requestFocus();
                lv.getSelectionModel().select(lv.getSelectionModel().getSelectedIndex() - 1);
                Photo.current = (Photo)lv.getSelectionModel().getSelectedItem();
            }
        };
        lastButton.setOnAction(last);
    }

    public void loadResults(){
        ObservableList<Photo> ol = FXCollections.observableArrayList();
        //consider sorting
        ol.addAll(SearchInputController.results);

        lv.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lv.setItems(ol);

        Map<String, Image> map = new HashMap<String, Image>();
        for(Photo p : ol){
            map.put(p.caption, p.thumbnail());
        }

        lv.setCellFactory(param -> new ListCell<Photo>() {
            private ImageView imageView = new ImageView();

            public void updateItem(Photo photo, boolean empty){
                super.updateItem(photo, empty);
                if(empty){
                    setText(null);
                    setGraphic(null);
                }
                else{
                    imageView.setImage(map.get(photo.caption));
                    setText(photo.caption);
                    setGraphic(imageView);
                }
            }
        });
        lv.getSelectionModel().select(0);
        Photo.current = (Photo)lv.getSelectionModel().getSelectedItem();

        //Set default display
        if(!lv.getItems().isEmpty()) {
            details.setText(((Photo) lv.getSelectionModel().getSelectedItem()).getDisplay());
            imageDisplay.setImage(((Photo) lv.getSelectionModel().getSelectedItem()).loadImage());
        }
    }
}
