package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Accounts;
import model.Album;
import model.Photo;
import model.Tag;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Controller for album.fxml
 * @author Daanyal and Ateeb
 */
public class AlbumController implements Initializable {

    @FXML ListView lv;
    @FXML Button logout;
    @FXML Button quitButton;
    @FXML Button backButton;
    @FXML Button addButton;
    @FXML Button deleteButton;
    @FXML Label details;
    @FXML ImageView imageDisplay;
    @FXML Button lastButton;
    @FXML Button nextButton;
    @FXML Button editCaption;
    @FXML Button editTag;
    @FXML Button copyButton;
    @FXML Button moveButton;

    final FileChooser fileChooser = new FileChooser();

    /**
     * Initializes and sets up events handlers and fx controls such as buttons and views
     * @param url url
     * @param rb resource bundle
     */
    public void initialize(URL url, ResourceBundle rb) {
        loadPhotos();

        fileChooser.setTitle("Add a Picture");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Images", "*.jpg", "*.png"));

        EventHandler<ActionEvent> add = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                File file = fileChooser.showOpenDialog((Stage) logout.getScene().getWindow());
                if (file != null) {
                    TextInputDialog captionprompt = new TextInputDialog();
                    captionprompt.setHeaderText("Enter a caption for this photo");
                    captionprompt.showAndWait();
                    String caption = captionprompt.getEditor().getText();

                    if(!Album.current.addImage(file.getAbsolutePath(), caption)){
                        Alert alertE = new Alert(Alert.AlertType.ERROR, "This photo already exists here.", ButtonType.OK);
                        alertE.showAndWait();
                    }

                    loadPhotos();
                }
            }
        };
        addButton.setOnAction(add);

        EventHandler<ActionEvent> delete = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Album.current.removeImage((Photo) lv.getSelectionModel().getSelectedItem());
                loadPhotos();
            }
        };
        deleteButton.setOnAction(delete);

        EventHandler<ActionEvent> move = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Photo p = (Photo) lv.getSelectionModel().getSelectedItem();
                Album.current.removeImage(p);

                ArrayList<Album> choices = Album.getAlbumsByUserForEditing(LoginController.user.toLowerCase());
                choices.remove(Album.current);

                if(choices.size() > 0) {
                    ChoiceDialog<Album> dialog = new ChoiceDialog<Album>(choices.get(0), choices);
                    dialog.setTitle("Move Photo");
                    dialog.setHeaderText("Choose Which Album to Move To");
                    dialog.setContentText("Choose your album:");
                    Optional<Album> result = dialog.showAndWait();

                    if(!result.isPresent()){
                        Album.current.addImage(p);
                        return;
                    }
                    Album copyTo = dialog.getSelectedItem();

                    if(copyTo.has(p)){
                        Alert alertE = new Alert(Alert.AlertType.CONFIRMATION, "This photo already exists there.", ButtonType.OK);
                        alertE.showAndWait();
                    }
                    else {
                        copyTo.addImage(p);
                        loadPhotos();
                        Alert alertE = new Alert(Alert.AlertType.CONFIRMATION, "Move Complete.", ButtonType.OK);
                        alertE.showAndWait();
                    }
                }
                else{
                    Album.current.addImage(p);
                    Alert alertExists = new Alert(Alert.AlertType.ERROR, "You have no other albums.", ButtonType.OK);
                    alertExists.showAndWait();
                }

            }
        };
        moveButton.setOnAction(move);

        EventHandler<ActionEvent> copy = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Photo p = (Photo) lv.getSelectionModel().getSelectedItem();

                ArrayList<Album> choices = Album.getAlbumsByUserForEditing(LoginController.user.toLowerCase());
                choices.remove(Album.current);

                if(choices.size() > 0) {
                    ChoiceDialog<Album> dialog = new ChoiceDialog<Album>(choices.get(0), choices);
                    dialog.setTitle("Copy Photo");
                    dialog.setHeaderText("Choose Which Album to Copy To");
                    dialog.setContentText("Choose your album:");
                    Optional<Album> result = dialog.showAndWait();

                    if(!result.isPresent()){
                        return;
                    }
                    Album copyTo = dialog.getSelectedItem();

                    if(copyTo.has(p)){
                        Alert alertE = new Alert(Alert.AlertType.CONFIRMATION, "This photo already exists there.", ButtonType.OK);
                        alertE.showAndWait();
                    }
                    else{
                        copyTo.addImage(p);
                        loadPhotos();
                        Alert alertE = new Alert(Alert.AlertType.CONFIRMATION, "Copy Complete.", ButtonType.OK);
                        alertE.showAndWait();
                    }
                }
                else{
                    Alert alertExists = new Alert(Alert.AlertType.ERROR, "You have no other albums.", ButtonType.OK);
                    alertExists.showAndWait();
                }
            }
        };
        copyButton.setOnAction(copy);

        EventHandler<ActionEvent> back = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Album.current = null;

                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/user.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        backButton.setOnAction(back);

        EventHandler<ActionEvent> backtologin = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/login.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        logout.setOnAction(backtologin);

        EventHandler<ActionEvent> quitnow = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) quitButton.getScene().getWindow();
                stage.close();
            }
        };
        quitButton.setOnAction(quitnow);

        EventHandler<ActionEvent> editTags = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {

                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/tag.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        editTag.setOnAction(editTags);

        EventHandler<ActionEvent> editcap = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Photo old = (Photo) lv.getSelectionModel().getSelectedItem();
                int i = lv.getSelectionModel().getSelectedIndex();

                TextInputDialog rename = new TextInputDialog();
                rename.setHeaderText("Enter the new caption");
                Optional<String> result = rename.showAndWait();

                if(!result.isPresent()){
                    return;
                }

                String newcap = rename.getEditor().getText();

                if(Album.current.has(newcap)){
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "You already have this caption. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
                else{
                    old.setCaption(newcap);
                }

                loadPhotos();
                lv.getSelectionModel().select(i);
                Album.writeAlbumsToFile(Album.albums);
            }
        };
        editCaption.setOnAction(editcap);

        //Link display
        lv.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Photo>() {
                    public void changed(ObservableValue<? extends Photo> ov,
                                        Photo old_val, Photo new_val) {
                        if(old_val != new_val && new_val != null) {
                            if (!lv.getItems().isEmpty()) {
                                details.setText(new_val.getDisplay());
                                imageDisplay.setImage(new_val.loadImage());
                            }
                        }
                    }
                });

        //Arrow keys
        EventHandler<ActionEvent> next = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                lv.requestFocus();
                lv.getSelectionModel().select(lv.getSelectionModel().getSelectedIndex() + 1);
                Photo.current = (Photo)lv.getSelectionModel().getSelectedItem();
            }
        };
        nextButton.setOnAction(next);
        EventHandler<ActionEvent> last = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                lv.requestFocus();
                lv.getSelectionModel().select(lv.getSelectionModel().getSelectedIndex() - 1);
                Photo.current = (Photo)lv.getSelectionModel().getSelectedItem();
            }
        };
        lastButton.setOnAction(last);

        lv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Photo>() {
            @Override
            public void changed(ObservableValue<? extends Photo> observable, Photo oldValue, Photo newValue) {
            Photo.current = (Photo)lv.getSelectionModel().getSelectedItem();
            }
        });

    }

    /**
     * Loads photos into imageView for display
     */
    public void loadPhotos(){
        if(Album.current.images == null){ return; }
        ObservableList<Photo> ol = FXCollections.observableArrayList();
        //consider sorting
        ol.addAll(Album.current.images);

        lv.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lv.setItems(ol);

        Map<String, Image> map = new HashMap<String, Image>();
        for(Photo p : ol){
            map.put(p.caption, p.thumbnail());
        }

        lv.setCellFactory(param -> new ListCell<Photo>() {
            private ImageView imageView = new ImageView();

            public void updateItem(Photo photo, boolean empty){
                super.updateItem(photo, empty);
                if(empty){
                    setText(null);
                    setGraphic(null);
                }
                else{
                    imageView.setImage(map.get(photo.caption));
                    setText(photo.caption);
                    setGraphic(imageView);
                }
            }
        });
        lv.getSelectionModel().select(0);
        Photo.current = (Photo)lv.getSelectionModel().getSelectedItem();

        //Set default display
        if(!lv.getItems().isEmpty()) {
            details.setText(((Photo) lv.getSelectionModel().getSelectedItem()).getDisplay());
            imageDisplay.setImage(((Photo) lv.getSelectionModel().getSelectedItem()).loadImage());
        }
    }

}
