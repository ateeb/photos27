package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SelectionMode;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;
import javafx.scene.control.ListView;
import javafx.scene.control.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

import java.util.ResourceBundle;

/**
 * Controller for tag.fxml
 * @author Daanyal and Ateeb
 */
public class TagController implements Initializable {

    @FXML Button backButton;
    @FXML ListView<String> lv;
    @FXML Button addTag;
    @FXML Button deleteTag;

    public Photo photo;

    /**
     * Initializes and sets up events handlers and fx controls such as buttons and views
     * @param url url
     * @param rb resource bundle
     */
    public void initialize(URL url, ResourceBundle rb) {
        photo = Photo.current;
        loadTags();


        EventHandler<ActionEvent> addtag = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {

                Stage window = (Stage) addTag.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/addTag.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        addTag.setOnAction(addtag);

        EventHandler<ActionEvent> delete = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                if (photo.tags.isEmpty()){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.initOwner(deleteTag.getScene().getWindow());
                    alert.setHeaderText("There are no tags to delete");
                    alert.showAndWait();
                }

                Tag t = new Tag("","", false);
                for (Tag tag : photo.tags) {
                    if (tag.toString().equalsIgnoreCase(lv.getSelectionModel().getSelectedItem())){
                        t = tag;
                    }
                }
                photo.tags.remove(t);
                t.photos.remove(photo);

                loadTags();
                Album.writeAlbumsToFile(Album.albums);
            }
        };
        deleteTag.setOnAction(delete);

        EventHandler<ActionEvent> back = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) backButton.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/album.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        backButton.setOnAction(back);
    }

    /**
     * Loads tags of photo into view
     */
    public void loadTags(){
        if(photo.tags.isEmpty()){
            ObservableList<String> ol = FXCollections.observableArrayList();
            lv.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            lv.setItems(ol);
            return;
        }
        ObservableList<String> ol = FXCollections.observableArrayList();

        for (Tag t : photo.tags) {
            ol.add(t.toString());
        }
        lv.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lv.setItems(ol);

        lv.getSelectionModel().select(0);
    }
}
