package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Accounts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller for login.fxml
 * @author Daanyal and Ateeb
 */
public class LoginController implements Initializable {

    @FXML Button login;
    @FXML TextField username;
    @FXML Button quitButton;

    public static String user;
    /**
     * Initializes and sets up events handlers and fx controls such as buttons and views
     * @param url url
     * @param rb resource bundle
     */
    public void initialize(URL url, ResourceBundle rb) {

        user = new String();

        EventHandler<ActionEvent> logmein = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                //Log in admin
                if(Accounts.getAdmins().contains(username.getCharacters().toString().toLowerCase())){
                    Stage window = (Stage) login.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("../view/admin.fxml"));
                    try {
                        window.setScene(new Scene(loader.load()));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                //Log in accounts
                else if(Accounts.getAccounts().contains(username.getCharacters().toString().toLowerCase())){
                    user = username.getCharacters().toString().toLowerCase();

                    Stage window = (Stage) login.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("../view/user.fxml"));
                    try {
                        window.setScene(new Scene(loader.load()));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                else{
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "Account not found. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
            }
        };
        login.setOnAction(logmein);

        EventHandler<ActionEvent> quitnow = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) quitButton.getScene().getWindow();
                stage.close();
            }
        };
        quitButton.setOnAction(quitnow);
    }
}
