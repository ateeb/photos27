package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Accounts;
import model.Album;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

/**
 * Controller for user.fxml
 * @author Daanyal and Ateeb
 */
public class UserController implements Initializable {

    @FXML ListView lv;
    @FXML Button deleteAlbum;
    @FXML Button logout;
    @FXML Button quitButton;
    @FXML Button addAlbum;
    @FXML TextField newalbum;
    @FXML Button renameAlbum;
    @FXML Button openAlbum;
    @FXML Button searchButton;

    /**
     * Initializes and sets up events handlers and fx controls such as buttons and views
     * @param url url
     * @param rb resource bundle
     */
    public void initialize(URL url, ResourceBundle rb) {
        loadAlbums();

        EventHandler<ActionEvent> addnewalbum = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Album t = Album.newAlbum(newalbum.getCharacters().toString(), LoginController.user);
                if(t == null){
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "You already have an album of this name. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
                else{
                    loadAlbums();
                }
            }
        };
        addAlbum.setOnAction(addnewalbum);
        newalbum.setOnAction(addnewalbum);

        EventHandler<ActionEvent> deletealbum = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                String selected = lv.getSelectionModel().getSelectedItem().toString();
                Album.deleteAlbum(selected, LoginController.user.toLowerCase());
                loadAlbums();
            }
        };
        deleteAlbum.setOnAction(deletealbum);

        EventHandler<ActionEvent> renamealbum = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                String old = lv.getSelectionModel().getSelectedItem().toString();

                TextInputDialog rename = new TextInputDialog();
                rename.setHeaderText("Enter the new name for the album");
                rename.showAndWait();

                String name = rename.getEditor().getText();

                if(name.equalsIgnoreCase("") || !Album.rename(old, name, LoginController.user)){
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "You already have an album of this name. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }

                loadAlbums();
            }
        };
        renameAlbum.setOnAction(renamealbum);

        EventHandler<ActionEvent> openalbum = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                ArrayList<Album> temp = Album.getAlbumsByUser(LoginController.user.toLowerCase());
                for(Album a : temp){
                    if(a.getName().equalsIgnoreCase(lv.getSelectionModel().getSelectedItem().toString())){
                        Album.current = a;
                        break;
                    }
                }

                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/album.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        openAlbum.setOnAction(openalbum);

        EventHandler<ActionEvent> backtologin = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/login.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        logout.setOnAction(backtologin);

        EventHandler<ActionEvent> quitnow = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) quitButton.getScene().getWindow();
                stage.close();
            }
        };
        quitButton.setOnAction(quitnow);

        EventHandler<ActionEvent> search = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/searchInput.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        searchButton.setOnAction(search);
    }

    /**
     * Load albums into listView
     */
    public void loadAlbums(){
        if(Album.getAlbumsByUser(LoginController.user.toLowerCase()) == null) { return; }
        ObservableList<Album> ol = FXCollections.observableArrayList();
        ArrayList<Album> sorted = Album.getAlbumsByUser(LoginController.user.toLowerCase());
        Collections.sort(sorted);
        ol.addAll(sorted);

        lv.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lv.setItems(ol);
        lv.getSelectionModel().select(0);
    }

}
