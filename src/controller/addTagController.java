package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;

import java.awt.*;
import java.awt.Label;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Controller for addTag.fxml
 * @author Daanyal and Ateeb
 *
 */
public class addTagController implements Initializable {
    @FXML TextField value;
    @FXML Button addTagType;
    @FXML Button cancelButton;
    @FXML ChoiceBox<String> tagType;
    @FXML Button submit;
    @FXML CheckBox multiple;

    /**
     * Initializes and sets up events handlers and fx controls such as buttons and views
     * @param url url
     * @param rb resource bundle
     */
    public void initialize(URL url, ResourceBundle rb) {
        loadTypes();

        EventHandler<ActionEvent> addtag = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                if (tagType.getValue() == null || tagType.getValue().length() <= 0 || value.getText() == null || value.getText().length() <= 0){
                    Alert invalid = new Alert(Alert.AlertType.ERROR);
                    invalid.initOwner(submit.getScene().getWindow());
                    invalid.setHeaderText("There are empty fields");
                    invalid.showAndWait();
                    return;

                }

                if (!multiple.isSelected()){
                    for (Tag t : Photo.current.tags){
                        if (t.getName().equalsIgnoreCase(tagType.getValue())){
                            Alert invalid = new Alert(Alert.AlertType.ERROR);
                            invalid.initOwner(submit.getScene().getWindow());
                            invalid.setHeaderText("You can't have more than one of these tags");
                            invalid.showAndWait();
                            return;
                        }
                    }
                }
                Tag tag = new Tag(tagType.getValue(), value.getText(), multiple.isSelected());

                for (Tag t : Photo.current.tags) {
                    if (t.getName().equalsIgnoreCase(tag.getName()) && t.getValue().equalsIgnoreCase(tag.getValue())) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.initOwner(submit.getScene().getWindow());
                        alert.setHeaderText("This tag already exists");
                        alert.showAndWait();
                        return;
                    }
                }
                Photo.current.tags.add(tag);
                tag.photos.add(Photo.current);

                Album.writeAlbumsToFile(Album.albums);
                Tag.writeTagsToFile(Tag.tags);

                Stage window = (Stage) cancelButton.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/tag.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        submit.setOnAction(addtag);

        EventHandler<ActionEvent> addTag = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                String newType;
                TextInputDialog td = new TextInputDialog("");
                td.setHeaderText("Enter new tag type. Remember to check the checkbox if you want to be able to use multiple of this new tag!");
                Optional<String> result = td.showAndWait();
                if (result.isPresent()) {
                    newType = result.get();
                    if(result.isPresent()){
                        Tag tag = new Tag( newType, "", multiple.isSelected());

                        for (Tag t: Tag.defaultTags){
                            if (t.getName().equalsIgnoreCase(newType)){
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.initOwner(submit.getScene().getWindow());
                                alert.setHeaderText("This tag already exists");
                                alert.showAndWait();
                                return;
                            }
                        }
                        for (Tag t: Tag.tags){
                            if (t.getName().equalsIgnoreCase(newType)){
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.initOwner(submit.getScene().getWindow());
                                alert.setHeaderText("This tag already exists");
                                alert.showAndWait();
                                return;
                            }
                        }

                        Tag.tags.add(tag);
                    }
                    Tag.writeTagsToFile(Tag.tags);
                    loadTypes();
                }


            }
        };
        addTagType.setOnAction(addTag);

        tagType.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                boolean isDefaultTag = false;
                for (Tag t : Tag.defaultTags){
                    if(tagType.getSelectionModel().getSelectedItem().equalsIgnoreCase(t.getName())){
                        isDefaultTag = true;
                        multiple.setSelected(t.multiple);
                    }
                }
                if (!isDefaultTag){
                    for (Tag t : Tag.tags){
                        if(tagType.getSelectionModel().getSelectedItem().equalsIgnoreCase(t.getName())){
                            multiple.setSelected(t.multiple);
                        }
                    }
                }
            }
        });

        EventHandler<ActionEvent> cancel= new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) cancelButton.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/tag.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        cancelButton.setOnAction(cancel);


    }

    /**
     * Loads types into ChoiceBox
     */
    public void loadTypes(){
        ObservableList<String> ol = FXCollections.observableArrayList();

        Tag.tags = Tag.getTags();
        for (Tag t : Tag.defaultTags) {
            ol.add(t.getName());
        }
        for (Tag t : Tag.tags ){
            ol.add(t.getName());
        }

        //check for new tags that are added
        tagType.setItems(ol);
    }


}
