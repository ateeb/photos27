package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Accounts;
import model.Album;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

/**
 * Controller for admin.fxml
 * @author Daanyal and Ateeb
 */
public class AdminController implements Initializable {

    @FXML Button logout;
    @FXML ListView lv;
    @FXML Button quitButton;
    @FXML Button addUser;
    @FXML TextField newuser;
    @FXML Button deleteUser;

    /**
     * Initializes and sets up events handlers and fx controls such as buttons and views
     * @param url url
     * @param rb resource bundle
     */
    public void initialize(URL url, ResourceBundle rb) {

        updateList();

        EventHandler<ActionEvent> addnewuser = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                if(newuser.getCharacters().toString().toLowerCase().equalsIgnoreCase("admin")){
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "A user of this name already exists. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
                else if(Accounts.addUser(newuser.getCharacters().toString())){
                    updateList();
                }
                else{
                    Alert alertExists = new Alert(Alert.AlertType.WARNING, "A user of this name already exists. Please try again.", ButtonType.OK);
                    alertExists.showAndWait();
                }
            }
        };
        addUser.setOnAction(addnewuser);
        newuser.setOnAction(addnewuser);

        EventHandler<ActionEvent> deleteuser = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                int selectedIndex = lv.getSelectionModel().getSelectedIndex();
                Accounts.removeUserByIndex(selectedIndex);
                updateList();
            }
        };
        deleteUser.setOnAction(deleteuser);

        EventHandler<ActionEvent> backtologin = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage window = (Stage) logout.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("../view/login.fxml"));
                try {
                    window.setScene(new Scene(loader.load()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        logout.setOnAction(backtologin);

        EventHandler<ActionEvent> quitnow = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Stage stage = (Stage) quitButton.getScene().getWindow();
                stage.close();
            }
        };
        quitButton.setOnAction(quitnow);
    }

    /**
     * Update list of accounts
     */
    public void updateList(){
        ObservableList<String> ol = FXCollections.observableArrayList();
        ArrayList<String> sorted = Accounts.getAccounts();
        Collections.sort(sorted);
        ol.addAll(sorted);

        lv.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lv.setItems(ol);
        lv.getSelectionModel().select(0);
    }
}
