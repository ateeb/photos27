package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Daanyal
 */
public class PhotosLibrary extends Application {

    /**
     * Set up primary stage where user is first directed when application launches
     * @param primaryStage First fxml page
     * @throws Exception for unable to boot
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        primaryStage.setTitle("Photo Library");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) { launch(args); }
}
