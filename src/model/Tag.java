package model;

import java.io.*;
import java.util.ArrayList;

/**
 * Represents a photo tag
 * @author Daanyal and Ateeb
 */
public class Tag implements Serializable {

    static final long serialVersionUID = 1L;

    public String name;
    public String value;
    public boolean multiple;

    private final static String tagsFile = "src/model/tags.dat";
    public ArrayList<Photo> photos = new ArrayList<Photo>();
    public static ArrayList<Tag> defaultTags = new ArrayList<Tag>() {
        {
            add(new Tag("Location","", false));
            add(new Tag("Person","", true));
        }
    };

    //for the extra tags created by user
    public static ArrayList<Tag> tags = new ArrayList<Tag>();

    public Tag(String type, String value, Boolean multiple){
        this.name = type;
        this.value = value;
        this.multiple = multiple;
    }

    /**
     * Writes new created Tags to tagsFile as a saving method
     * @param tags List of Tags you want to save
     */
    public static void writeTagsToFile(ArrayList<Tag> tags){
        try{
            FileOutputStream fos = new FileOutputStream(tagsFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(tags);
            oos.close();
            fos.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    /**
     * Gets list of Tags from tagsFile
     * @return list of Tags from file
     */
    public static ArrayList<Tag> getTags() {
        tags = new ArrayList<Tag>();

        try {
            FileInputStream fis = new FileInputStream(tagsFile);
            ObjectInputStream ois = new ObjectInputStream(fis);

            tags = (ArrayList<Tag>) ois.readObject();

            ois.close();
            fis.close();
        } catch (EOFException e){
            tags = new ArrayList<Tag>();
        } catch (FileNotFoundException e) {
            return tags;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return tags;
    }

    /**
     * Gets list of Tags
     * @return list of Tags from  memory
     */
    public static ArrayList<Tag> getTagsForEdit(){
        return tags;
    }

    /**
     * Gets tag type
     * @return tag name/type
     */
    public String getName() {
        return name;
    }

    /**
     * Set tag type
     * @param type new type of tag
     */
    public void setName(String type) {
        this.name = type;
    }

    /**
     * Get tag value
     * @return tag value
     */
    public String getValue() {
        return value;
    }

    /**
     * Set tag value
     * @param value new vale of tag
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Creates a string representation of tag
     * @return tag as a string
     */
    public String toString(){
        return this.getName() + ": " + this.getValue();
    }

    /**
     * Returns whether two albums are the same
     * @param o the tag being compared
     * @return Whether the tags are equal or not
     */
    public boolean equals(Object o){
        if(o == null || ! (o instanceof Tag)){
            return false;
        }

        if(o == this){
            return true;
        }

        Tag a = (Tag) o;
        if(a.name.equalsIgnoreCase(name) && a.value.equalsIgnoreCase(value)){
            return true;
        }

        return false;
    }
}
