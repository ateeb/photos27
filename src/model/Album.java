package model;

import controller.LoginController;
import javafx.scene.image.Image;

import java.io.*;
import java.util.ArrayList;

/**
 * Represents an album
 * @author Daanyal and Ateeb
 */
public class Album implements Serializable, Comparable<Album> {

    public static Album current;

    public static ArrayList<Album> albums = new ArrayList<Album>();
    private final static String albumsFile = "src/model/albums.dat";

    static final long serialVersionUID = 1L;

    private String name;
    private String owner;
    public ArrayList<Photo> images;

    public Album(String name, String user){
        images = new ArrayList<Photo>();
        this.name = name;
        owner = user;
    }

    /**
     * Gets albums name
     * @return albums name as a String
     */
    public String getName(){
        return name;
    }

    /**
     * Gets the number of photos in the album
     * @return The size the the list of photos in the album
     */
    public int getSize(){
        return images.size();
    }

    /**
     * Gets owner of the album
     * @return the name of the owner as a String
     */
    public String getOwner(){
        return owner;
    }

    /**
     * Compares album to another album
     * @param a album being compared
     * @return 1 or 0 depending on whether the albums are the same
     */
    public int compareTo(Album a){
        return this.getName().compareTo(a.getName());
    }

    /**
     * Creates a string repersentation of the album
     * @return string repersentation of album
     */
    public String toString(){
        return getName();
    }

    /**
     * Adds a photo to the album
     * @param filepath filepath of the photo
     * @param caption caption for photo
     * @return Whether the photo was successfully added
     */
    public boolean addImage(String filepath, String caption){
        if(has(new Photo(filepath, caption))){
            return false;
        }
        images.add(new Photo(filepath, caption));
        writeAlbumsToFile(Album.albums);
        return true;
    }

    /**
     * Adds a photo to the album
     * @param photo photo to be added
     */
    public void addImage(Photo photo){
        images.add(photo);
        writeAlbumsToFile(Album.albums);
    }

    /**
     * Removes photo from album
     * @param photo photo to be removed
     */
    public void removeImage(Photo photo){
        images.remove(photo);
        writeAlbumsToFile(Album.albums);
    }

    /**
     * Tells whether there is a photo with that caption in the album
     * @param caption caption you are searching for
     * @return whether or not a photo with that caption was found
     */
    public boolean has(String caption){
        for(Photo p : images){
            if(p.caption.equalsIgnoreCase(caption)){
                return true;
            }
        }

        return false;
    }

    /**
     * Tells whether that photo exists in the album
     * @param p photo you are searching for
     * @return Whether the photo was found
     */
    public boolean has(Photo p){
        for(Photo o : images){
            if(o.filepath.equalsIgnoreCase(p.filepath)){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether two albums are the same
     * @param o the album being compared
     * @return Whether the albums are equal or not
     */
    public boolean equals(Object o){
        if(o == null || ! (o instanceof Album)){
            return false;
        }

        if(o == this){
            return true;
        }

        Album a = (Album) o;
        if(a.getOwner().equalsIgnoreCase(owner) && a.getName().equalsIgnoreCase(name) && a.images.size() == images.size()){
            return true;
        }

        return false;
    }

    //Static stuff

    /**
     * Get all albums of a specific user from the data file
     * @param user user's name
     * @return A list of a user's albums
     */
    public static ArrayList<Album> getAlbumsByUser(String user){
        getAlbums();
        ArrayList<Album> useralbums = new ArrayList<Album>();

        for(Album a : albums){
            if(a.owner.equalsIgnoreCase(user)){
                useralbums.add(a);
            }
        }

        return useralbums;
    }

    /**
     * Gets albums by a specific user
     * @param user user's name
     * @return a list the user's albums
     */
    public static ArrayList<Album> getAlbumsByUserForEditing(String user){

        ArrayList<Album> useralbums = new ArrayList<Album>();

        for(Album a : albums){
            if(a.owner.equalsIgnoreCase(user)){
                useralbums.add(a);
            }
        }

        return useralbums;
    }

    /**
     * Writes albums to albumsFile as a method saving them
     * @param albums list albums you want to write to file
     */
    public static void writeAlbumsToFile(ArrayList<Album> albums){
        try{
            FileOutputStream fos = new FileOutputStream(albumsFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(albums);
            oos.close();
            fos.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    /**
     * Gets album list from albumsFile
     * @return list of albums saved in albumsFile
     */
    public static ArrayList<Album> getAlbums() {
        albums = new ArrayList<Album>();

        try {
            FileInputStream fis = new FileInputStream(albumsFile);
            ObjectInputStream ois = new ObjectInputStream(fis);

            albums = (ArrayList<Album>) ois.readObject();

            ois.close();
            fis.close();
        } catch (EOFException e){
            albums = new ArrayList<Album>();
        } catch (FileNotFoundException e) {
            return albums;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return albums;
    }

    /**
     * Delete album for a given user
     * @param name name of album
     * @param user name of user
     */
    public static void deleteAlbum(String name, String user){
        for(Album a : albums){
            if(a.getName().equalsIgnoreCase(name) && a.getOwner().equalsIgnoreCase(user)){
                albums.remove(a);
                writeAlbumsToFile(albums);
                getAlbums();
                return;
            }
        }
    }

    /**
     * Tells whether user has an album of given name
     * @param name name of album
     * @param user name of user
     * @return Whether user has album or not
     */
    public static boolean exists(String name, String user){
        for(Album a : albums){
            if(a.getName().equalsIgnoreCase(name) && a.getOwner().equalsIgnoreCase(user)){
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a new album for a user
     * @param name name of album
     * @param user name of user
     * @return new album created
     */
    public static Album newAlbum(String name, String user){
        if(exists(name, user)){
            return null;
        }
        else{
            Album a = new Album(name, user.toLowerCase());
            albums.add(a);
            writeAlbumsToFile(albums);
            return a;
        }
    }

    /**
     * Renames an album
     * @param old old name of album
     * @param name new name of album
     * @param user name of user
     * @return whether the name change was successful
     */
    public static boolean rename(String old, String name, String user){
        if(!old.equalsIgnoreCase(name) && exists(name, user)){
            return false;
        }
        else{
            deleteAlbum(old, user);
            newAlbum(name, user);
            return true;
        }
    }
}
