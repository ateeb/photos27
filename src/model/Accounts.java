package model;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Reperents an Account
 * @author Daanyal and Ateeb
 */
public class Accounts {

    private static ArrayList<String> accounts;
    private static ArrayList<String> admins;

    private final static String accountsFile = "src/model/accounts.dat";
    private final static String adminsFile = "src/model/admins.dat";

    static final long serialVersionUID = 1L;

    /**
     * Gets the list of Accounts from the accountsFile
     * @return A list of normal user names
     */
    public static ArrayList<String> getAccounts() {
        accounts = new ArrayList<String>();

        try{
            FileInputStream fis = new FileInputStream(accountsFile);
            ObjectInputStream ois = new ObjectInputStream(fis);

            accounts = (ArrayList<String>) ois.readObject();

            ois.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return accounts;
    }

    /**
     * Gets the list of admins from the adminList
     * @return a list of admin account names
     */
    public static ArrayList<String> getAdmins() {
        admins = new ArrayList<String>();

        try{
            FileInputStream fis = new FileInputStream(adminsFile);
            ObjectInputStream ois = new ObjectInputStream(fis);

            admins = (ArrayList<String>) ois.readObject();

            ois.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return admins;
    }

    /**
     * Creates default accounts as starting base of users
     * @return An arraylist of default accounts names
     */
    public static ArrayList<String> createDefaultAccounts(){
        admins = new ArrayList<String>();
        admins.add("admin");

        writeAdminsToFile(admins);

        accounts = new ArrayList<String>();
        accounts.add("ateeb");

        writeAccountsToFile(accounts);

        return admins;
    }

    /**
     * Writes admins list to adminsFile
     * @param admins list of admins
     */
    public static void writeAdminsToFile(ArrayList<String> admins){
        try{
            FileOutputStream fos = new FileOutputStream(adminsFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(admins);
            oos.close();
            fos.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    /**
     * Writes accounts to accountsFile
     * @param accounts list of accounts
     */
    public static void writeAccountsToFile(ArrayList<String> accounts){
        try{
            FileOutputStream fos = new FileOutputStream(accountsFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(accounts);
            oos.close();
            fos.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    /**
     * Adds a user to the account list
     * @param acc user name to be addede
     * @return whether user was successfully added or not
     */
    public static boolean addUser(String acc){

        if(getAccounts().contains(acc.toLowerCase())){
            return false;
        }

        ArrayList<String> accs = getAccounts();
        accs.add(acc.toLowerCase());

        writeAccountsToFile(accs);

        return true;
    }

    /**
     * Removes user of given index from account list, and updates file
     * @param index of user
     */
    public static void removeUserByIndex(int index){
        Collections.sort(accounts);
        accounts.remove(index);

        writeAccountsToFile(accounts);
    }
}
