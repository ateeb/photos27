package model;

import javafx.scene.image.Image;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Represents a photo
 * @author Daanyal and Ateeb
 */
public class Photo implements Serializable {

    static final long serialVersionUID = 1L;

    public static Photo current;
    public String filepath;
    public String caption;
    public ArrayList<Tag> tags;
    public LocalDate timestamp;

    public Photo(String filepath, String caption){
        this.filepath = filepath;
        this.caption = caption;
        tags = new ArrayList<Tag>();
        timestamp = LocalDate.now();
    }

    /**
     * Makes a display string with photo information
     * @return formatted display string
     */
    public String getDisplay(){
        return caption + "\n" + timestamp + "\n" + tags.toString();
    }

    /**
     * Gets image object for photo
     * @return image object for photo
     */
    public Image loadImage(){
        return new Image("file:///" + filepath, 0, 200, true, false);
    }

    /**
     * Makes thumbnail size image
     * @return thumbnail size image object
     */
    public Image thumbnail(){
        return new Image("file:///" + filepath, 50, 50, true, false);
    }

    /**
     * Sets a caption for photo
     * @param cap caption for photo
     */
    public void setCaption(String cap){
        caption = cap;
    }

    /**
     * Checks if a photo is associated with a Tag - Value
     * @param t Tag to check if present
     * @return whether the tag with the specified type and value exist in the photo's tag list
     */
    public boolean hasTag(Tag t){
        if(tags.contains(t)){
            return true;
        }

        return false;
    }

    /**
     * Creates string representation for photo
     * @return string representation of photo
     */
    public String toString(){
        return caption;
    }

}
